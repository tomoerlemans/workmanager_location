import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workmanager/workmanager.dart';
import 'package:location/location.dart';

void callbackDispatcher() {
  Workmanager.executeTask((task, inputData) {
    sharedPrefTest();
    print(
        "Native called background task at ${DateTime.now().toString()}"); //simpleTask will be emitted here.
    getLocation();

    return Future.value(true);
  });
}

void sharedPrefTest() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int counter = (prefs.getInt('counter') ?? 0) + 1;
  print('Pressed $counter times.');
  await prefs.setInt('counter', counter);
}

void main() {
  print("m1");
  WidgetsFlutterBinding.ensureInitialized();
  Workmanager.initialize(
      callbackDispatcher, // The top level function, aka callbackDispatcher
      isInDebugMode:
          false // If enabled it will post a notification whenever the task is running. Handy for debugging tasks
      );
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                Workmanager.registerPeriodicTask(
                    "1", "simpleTask"); //Android only (see below)
              },
              child: Text("Start"),
            ),
            RaisedButton(
              onPressed: () {
                getLocation();
              },
              child: Text("Get location"),
            ),
          ],
        ),
      ),
    );
  }
}

void getLocation() async {
  LocationData currentLocation;
  var location = new Location();
  try {
    currentLocation = await location.getLocation();
  } on Exception catch (e) {
    print(e);
    currentLocation = null;
  }

  print("location background:");
  print(currentLocation);
  print(currentLocation.latitude);
  print(currentLocation.longitude);
  print(currentLocation.accuracy);
  print(currentLocation.altitude);
  print(currentLocation.speed);
  print(currentLocation.speedAccuracy);
  print(currentLocation.heading);
  print(currentLocation.time);
}
